import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Finance',
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final Future<PaymentModel> paymentModel;

  MyHomePage({
    Key key,
    this.paymentModel,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //MyHomePage({Key key, this.PaymentModel}) : super(key: key);

  TextEditingController numberController = new TextEditingController();
  TextEditingController amountController = new TextEditingController();
  TextEditingController billingController = new TextEditingController();

//  TextEditingController emailController = new TextEditingController();
//  TextEditingController emailController = new TextEditingController();

  var pressedMTN = false; // This is the press variable
  var pressedTIGO = false; // This is the press variable
  var pressedAirtel = false; // This is the press variable
  var pressedVoda = false; // This is the press variable
  Color _myColorTigo = Colors.transparent;
  Color _myColorAirtel = Colors.transparent;
  Color _myColorVoda = Colors.transparent;
  Color _myColorMTN = Colors.transparent;

  String network_type = "";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Payment'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                      onTap: () {
                        Fluttertoast.showToast(
                            msg: "This is MTN",
                            gravity: ToastGravity.BOTTOM,
                            toastLength: Toast.LENGTH_LONG);

                        setState(() {
                          pressedMTN = true;
                          pressedTIGO = false;
                          pressedAirtel = false;
                          pressedVoda = false;

                          if (pressedMTN == true) {
                            network_type = "MTN";
                            _myColorTigo = Colors.transparent;
                            _myColorAirtel = Colors.transparent;
                            _myColorVoda = Colors.transparent;
                            _myColorMTN = Colors.amber;
                          }
                        });
                      },
                      child: Container(
                        color: _myColorMTN,
                        child: Image.asset(
                          "assets/mtn.png",
                          height: 70.0,
                          width: 70.0,
                        ),
                      )),
                ),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: "This is TIGO",
                          gravity: ToastGravity.BOTTOM,
                          toastLength: Toast.LENGTH_LONG);
                      setState(() {
                        pressedMTN = false;
                        pressedTIGO = true;
                        pressedAirtel = false;
                        pressedVoda = false;

                        if (pressedTIGO == true) {
                          network_type = "TIG";
                          _myColorTigo = Colors.blue;
                          _myColorAirtel = Colors.transparent;
                          _myColorVoda = Colors.transparent;
                          _myColorMTN = Colors.transparent;
                        }
                      });
                    },
                    child: Container(
                      color: _myColorTigo,
                      child: Image.asset(
                        "assets/tigocash.png",
                        height: 70.0,
                        width: 70.0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: "This is VODAFONE",
                          gravity: ToastGravity.BOTTOM,
                          toastLength: Toast.LENGTH_LONG);

                      setState(() {
                        pressedMTN = false;
                        pressedTIGO = false;
                        pressedAirtel = false;
                        pressedVoda = true;

                        if (pressedVoda == true) {
                          network_type = "VOD";
                          _myColorTigo = Colors.transparent;
                          _myColorAirtel = Colors.transparent;
                          _myColorVoda = Colors.red;
                          _myColorMTN = Colors.transparent;
                        }
                      });
                    },
                    child: Container(
                      color: _myColorVoda,
                      child: Image.asset(
                        "assets/vodacash.png",
                        height: 70.0,
                        width: 70.0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: "This is AIRTEL",
                          gravity: ToastGravity.BOTTOM,
                          toastLength: Toast.LENGTH_LONG);
                      setState(() {
                        pressedMTN = false;
                        pressedTIGO = false;
                        pressedAirtel = true;
                        pressedVoda = false;

                        if (pressedAirtel == true) {
                          network_type = "AIR";
                          _myColorTigo = Colors.transparent;
                          _myColorAirtel = Colors.redAccent;
                          _myColorVoda = Colors.transparent;
                          _myColorMTN = Colors.transparent;
                        }
                      });
                    },
                    child: Container(
                      color: _myColorAirtel,
                      child: Image.asset(
                        "assets/airtel.png",
                        height: 70.0,
                        width: 70.0,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: numberController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              decoration: InputDecoration(
                  labelText: 'Customer Number',
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green))),
            ),
            TextField(
              controller: amountController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                  labelText: 'Amount',
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green))),
            ),
            TextField(
              controller: billingController,
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              decoration: InputDecoration(
                  labelText: 'Billing Info',
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green))),
            ),
            SizedBox(height: 20.0),
            Container(
              height: 40.0,
              child: Material(
                borderRadius: BorderRadius.circular(20.0),
                shadowColor: Colors.greenAccent,
                color: Colors.green,
                elevation: 7.0,
                child: GestureDetector(
                  onTap: () {
                    Fluttertoast.showToast(
                        msg: "Please wait...",
                        gravity: ToastGravity.CENTER,
                        toastLength: Toast.LENGTH_LONG);
                    _makePayment();
                  },
                  child: Center(
                    child: Text(
                      'Pay',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _makePayment() {
    PaymentModel paymentModel = new PaymentModel(
        amount: amountController.text,
        appointment_title: 'Home Care',
        billing_info_id: billingController.text,
        customer_number: numberController.text,
        network: network_type,
        trans_type: 'DR');

    createPost(paymentModel, 'http://5.153.40.138:6099/make_payment')
        .then((response) {
      // Map data = JSON.decode(response);
      final result = json.decode(response.body);

      // if(response.statusCode == 200) {
      if (result['resp_code'] == '000') {
        _showDialog(result['resp_desc']);
      } else {
        _showDialog((result['resp_desc']));
      }

      print('Body ${response.body}');
      print("Resp ${result['resp_code']}");
      //    }
      //     else
      //       print(response.statusCode);
    }).catchError((error) {
      print('error : $error');
    });
  }

  void _showDialog(String custom_content) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Response"),
          content: new Text(custom_content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<dynamic> createPost(PaymentModel paymentModel, String url) async {
    final response = await http.post(url,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: json.encode(paymentModel) //postToJson(paymentModel)
        );

    print('***************${response.request.toString()}');
    print('***************${response.statusCode.toString()}');
    print('***************${response.body.toString()}');

    return response;
  }
}

class PaymentModel {
  String billing_info_id;
  String amount;
  String network;
  String trans_type;
  String customer_number;
  String appointment_title;

  PaymentModel(
      {this.billing_info_id,
      this.amount,
      this.network,
      this.trans_type,
      this.customer_number,
      this.appointment_title});

  factory PaymentModel.fromJson(Map<String, dynamic> json) {
    return PaymentModel(
        billing_info_id: json['billing_info_id'],
        appointment_title: json['appointment_title'],
        amount: json['amount'],
        network: json['network'],
        trans_type: json['trans_type'],
        customer_number: json['customer_number']);
  }

  Map<String, dynamic> toJson() => {
        "billing_info_id": billing_info_id,
        "amount": amount,
        "network": network,
        "trans_type": trans_type,
        "appointment_title": appointment_title,
        "customer_number": customer_number
      };

  PaymentModel postFromJson(String str) {
    final jsonData = json.decode(str);
    return PaymentModel.fromJson(jsonData);
  }

  String postToJson(PaymentModel data) {
    final dyn = data.toJson();
    return json.encode(dyn);
  }
}
